loginOnly = "";

var loggedInProfile = {};

rowObjArrTemp = [{date:"2018/05/18",begin:"650",end:"750",distance:"50"},
{ date: "2018/05/15", begin: "750", end: "800", distance: "50" },
{ date: "2018/05/16", begin: "850", end: "900", distance: "50" },
{ date: "2018/05/17", begin: "950", end: "1000", distance: "50" },
{ date: "2018/05/18", begin: "1050", end: "1100", distance: "50" }];

var rowCount = 0;
function RemoveItem(itemRow) {
    $("#claimRow" + itemRow).remove();
}

function AddRow(rowObj) {
    rowCount++;
    var claimsRow = "<tr id='claimRow" + rowCount + "'>";
    claimsRow += "<td>" + rowObj.date + "</td>";
    claimsRow += "<td>" + rowObj.distance + "km</td>";
    claimsRow += "<td>" + rowObj.begin + ' - ' + rowObj.end + "</td>";
    claimsRow += "<td><button  class='submit-button' onclick='RemoveItem(" + rowCount + ");'>REMOVE</button></td>";
    claimsRow += "</tr>";

    $("#claimsTable").append(claimsRow);
}

function BuildTable(rowObjArr) {
    for (var i = 0; i < rowObjArr.length; i++) {
        AddRow(rowObjArr[i]);
    }
}




function claimsSubmit() {
    var rowObj = {};
    rowObj.date = $("#txtDate").val();
    rowObj.begin = $("#txtBegin").val();
    rowObj.end = $("#txtEnd").val();
    rowObj.distance = $("#txtDistance").val() || ($("#txtEnd").val() - $("#txtBegin").val());
    AddRow(rowObj);
    SubmitRow(rowObjArrTemp);
};


function SubmitRow(rowObjArrTemp) {
    $.post("/data.json", rowObjArrTemp, function (data) {
      
    });
}


function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    loggedInProfile = googleUser.getBasicProfile();
    
    $.get('data.json', function (data) {
        rowObjArrTemp = data[loggedInProfile.getEmail()];
    }, 'json');



    $("#loginOnly").css("display", "block");
    $("#logoutOnly").css("display", "none");
};


//gapi.client.sheets.spreadsheets.values.get({
//    spreadsheetId: spreadsheetId,
//    range: range
//}).then((response) => {
//    var result = response.result;
//    var numRows = result.values ? result.values.length : 0;
//    console.log(`${numRows} rows retrieved.`);
//});