(ns claims.db
    (:require [clojure.java.jdbc :as jdbc]))
  
  (def db-spec {:dbtype "h2" :dbname "./claims"})
  
  (def add-trip
    [userid date distance startkm endkm]
    (let [results (jdbc/insert! db-spec :trips {userid: userid :date date :distance distance :startkm startkm :endkm endkm})]
        (assert (= (count results) 1))
        (first (vals (first results)))))

(defn get-trips
    [userid]
    (let [results (jdbc/query db-spec
                                ["select date, distance, startkm, endkm from trips where userid = ?" userid])]
        (assert (= (count results) 1))
        (first results)))