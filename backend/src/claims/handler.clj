  (ns claims.handler
    (:require [claims.views :as views]
              [compojure.core :refer :all]
              [compojure.route :as route]
              [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))
  
  (defroutes app-routes
    (GET "/"
         []
         (views/home-page))

    (GET "/add-trip"
          []
          (views/add-trip-page))

    (GET "/trip/:userid"
         [userid]
         (views/trips userid))
  
  (def app
    (wrap-defaults app-routes site-defaults))
