(ns my-webapp.views
    (:require [claims.db :as db]
              [clojure.string :as str]
              [hiccup.page :as page]
              [ring.util.anti-forgery :as util]))
  
  (defn gen-page-head
    [title]
    [:head
     [:title (str "Trips: " title)]
     (page/include-css "/css/styles.css")])
  
  
  (defn home-page
    []
    (page/html5
     (gen-page-head "Home")
     header-links
     <html lang="en">
<head>
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="825979528030-s9uh2pfdufeaahocaonqqgae63q641f4.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js" async defer></script>
</head>
<body>
<div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
<script src="index.js"></script>
</body>
</html>))
  
  (defn add-location-page
    []
    (page/html5
     (gen-page-head "Add a Location")
     <html>
<head>
    <title>
    </title>
    <!-- CSS -->
    <link rel="stylesheet" href="index.css">
</head>
<body>
    <header class="header">
        <img class="logo" src=".\Icons\Retro Rabbit Logos\SVG\RetroRabbit-Logo-Green-Stacked.svg" />
    </header>
    <div class="content-background padding-v-15-xs">
        <div class="container">
            <div class="new-claim">
                <div class="heading padding-v-15-xs">TRAVEL CLAIMS</div>
                <div class="card-container">
                    <div class="card-header">Start a new claim</div>
                    <div class="card-body">
                        <div class="input-container">
                            <div class="label">Date</div>
                            <div class="input"><input type="date" id="txtDate"></div>
                        </div>
                        <div class="input-container">
                            <div class="label">Begin Kilo's</div>
                            <div class="input"><input type="number" id="txtBegin"></div>
                        </div>
                        <div class="input-container">
                            <div class="label">End Kilo's</div>
                            <div class="input"><input type="number" id="txtEnd"></div>
                        </div>
                        <div class="input-container">
                            <div class="label">Distance Traveled</div>
                            <div class="input"><input type="number" id="txtDistance"></div>
                        </div>
                        <button class="submit-button" id="btnSubmitClaim" onclick="claimsSubmit()">Submit</button>
                    </div>
                </div>
            </div>
            <div class="logged-claims margin-v-15-xs">
                <div class="card-container">
                    <div class="card-header">Previous Claims</div>
                    <div class="card-body">
                        <table id="claimsTable">
                            <thead>

                            <th>Date</th>
                            <th>Distance</th>
                            <th>Odometer reading</th>
                            <th>Actions</th>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <footer class="footer"></footer>
    <!-- JS -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <script src="index.js"></script>
</body>
</html>))