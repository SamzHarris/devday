# claims

DB SetUp
(require '[clojure.java.jdbc :as jdbc])
(jdbc/with-db-connection [conn {:dbtype "h2" :dbname "./claims"}]

  (jdbc/db-do-commands conn
    (jdbc/create-table-ddl :trips
      [[:id "bigint primary key auto_increment"]
       [:userid "varchar"]
       [:date "date"]
       [:distance "float"]
       [:startkm "float"]
       [:endkm "float"]])))

  (jdbc/insert! conn :trips
    {userid: "tester@retrorabbit.co.za" :date 2018/05/18 :distance 45.00 :startkm 7500.00 :endkm 7545.00}))

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2018 FIXME
